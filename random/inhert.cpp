#include<iostream>
using namespace std;
class Car{
public:
	void wheel()
	{
		cout<<"Car has wheel"<<endl;
	}
};
class Bike:public Car
{
public:
	void wheel()
	{
		cout<<"Bike has two wheels"<<endl;
	}

};
class Train : public Car
{
public:
	void wheel()
	{
		cout<<"Train has many wheels"<<endl;
	}
};
int main()
{
	Bike ob;
	Train ob1;
	// ob.wheel();
	ob.wheel();
	ob1.wheel();

}