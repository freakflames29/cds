#include <iostream>
using namespace std;
struct node
{
	int data;
	node* next;
};
void trav(node* ptr)
{
	while(ptr!=NULL)
	{
		cout<<ptr->data<<"->";
		ptr=ptr->next;
	}
}
int main()
{
	node* head=new node();
	node* sec=new node();
	node* third=new node();


	head->data=50;
	head->next=sec;

	sec->data=60;
	sec->next=third;

	third->data=70;
	third->next=NULL;

	cout<<"Traversing the linked list"<<endl;
	trav(head);



}