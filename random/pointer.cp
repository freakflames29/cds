#include <iostream>
using namespace std;
int main()
{
	int a=50;
	int *ptr=&a;
	
	cout<<"The value of a is "<<a<<endl;
	cout<<"The address of a and value of &a is "<<&a<<endl;
	cout<<"The value of ptr "<<ptr<<endl;
	cout<<"The value of 'a' using pointer(*ptr) "<<*ptr<<endl;

	*ptr=100;
	cout<<"Now a is "<<a<<endl;
	cout<<"Now *ptr is "<<*ptr<<endl;
}