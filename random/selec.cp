#include <iostream>
using namespace std;
int main()
{
	int min=0,comp=0,swap=0;
	// int a[5]={5,6,1,9,2};
	// int a[5]={5,4,3,2,1};
	int a[5]={1,2,3,4,6};
	int size=sizeof(a)/sizeof(a[0]);
	for (int i = 0; i < size; ++i)
	{
		min=a[i];
		int loc=i;
		for (int j = i+1; j < size; ++j)
		{
			comp++;
			if(a[j]<min)
			{
				min=a[j];
				loc=j;
			}
		}
		if(i!=loc)
		{
			int temp=a[i];
			a[i]=min;
			a[loc]=temp;
			swap++;
		}
	}
	cout<<"The sorted list by selection sort"<<endl;
	for (int i = 0; i < size; ++i)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;
	cout<<"Total number of comparision "<<comp<<endl;
	cout<<"Total number of swaps "<<swap<<endl;
}