def findmin(a,n):
	res=a[0]
	for i in range(0,n):
		res=min(res,a[i])
	return res
def findmax(a,n):
	res=a[0]
	for i in range(0,n):
		res=max(res,a[i])
	return res

def main():
	a=[1,6,3,9]
	print("Minimum is %d"%findmin(a,len(a)))
	print("Maximum is %d"%findmax(a,len(a)))
main()