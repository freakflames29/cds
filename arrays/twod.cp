#include <iostream>
using namespace std;
int main()
{

	cout<<"Enter row number"<<endl;
	int r;
	cin>>r;
	cout<<"Enter the column number"<<endl;
	int c;
	cin>>c;
	int a[r][c];
	cout<<"Enter the array elements"<<endl;
	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			cin>>a[i][j];
		}
	}
	cout<<"The matrix"<<endl;
	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			cout<<a[i][j]<<" ";
		}
		cout<<endl;
	}
	cout<<endl;
}