#include <iostream>
# include <stdlib.h>
using namespace std;

class cl
{
	int A[100],h,count=0;
	public:		
		cl()
		{
			cout<<"* PROGRAMME FOR COUNTING NO OF COMPARISON IN INSERTION SORT  *";
		}
		void input()
		{
			cout<<"\n ENTER THE LENGTH"<<" ";
			cin>>h;
			for (int i=1; i<=h; i++)
			{
				cin>>A[i];
			}
			A[0]=-999;
		}

		void InsertionSort()
		{
			int i, j;
			int key;
			for (i = 1; i <=h; i++)
			{
				key = A[i];
				j = i - 1;
				while ( A[j] > key)
				{	
					A[j + 1] = A[j];
					j = j - 1;
					count++;
				}
				A[j + 1] = key;
				count++;
			} 
		}
		
		void output(){
			cout<<"Array elements after sorting";
			for(int i=1;i<=h;i++){
				cout<<"  "<<A[i];
			}
			cout<<"\nThe total no of comparison:-"<<count;
		}

};
int main()
{
	cl ob;
	ob.input();
	ob.InsertionSort();
	ob.output();
}