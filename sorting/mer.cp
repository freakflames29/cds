#include <iostream>
using namespace std;
void merge(int *a,int low,int mid,int up)
{
	int b[(low+up)+1];
	int i=low,j=mid+1,k=low;
	while(i<=mid && j<=up)
	{
		if(a[i]<a[j])
		{
			b[k]=a[i];
			i++;
		}
		else
		{
			b[k]=a[j];
			j++;
		}
		k++;
	}
	while(i<=mid)
	{
		b[k]=a[i];
		i++;
		k++;
	}
	while(j<=up)
	{
		b[k]=a[j];
		j++;
		k++;
	}
	int p=low;
	while(p<k)
	{
		a[p]=b[p];
		p++;
	}
}
void divide(int *a,int low,int up)
{
	if(low<up)
	{
		int mid=(low+up)/2;
		divide(a,low,mid);
		divide(a,mid+1,up);
		merge(a,low,mid,up);
	}
}
int main()
{
	cout<<"Enter the length"<<endl;
	int n;
	cin>>n;
	int a[n];
	for (int i = 0; i < n; ++i)
	{
		cin>>a[i];
	}
	cout<<"The sorted array"<<endl;
	divide(a,0,n);
	for (int i = 0; i < n; ++i)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;
}