#include <iostream>
#include <stdlib.h>
using namespace std;
int main()
{
	int a[100];
	int comp=0;
	for (int i = 0; i < 100; ++i)
	{
		a[i]=30+rand()%1000;
	}
	cout<<"The array is "<<endl;
	for (int i = 0; i < 100; ++i)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;
	for (int i = 1; i < 100; ++i)
	{
		int key=a[i];
		int j=i-1;
		while(j>=0&&a[j]>key)
		{
			a[j+1]=a[j];
			j--;
			comp+=2;
		}
		a[j+1]=key;
	}
	cout<<"The sorted array"<<endl;
	for (int i = 0; i < 100; ++i)
	{
		cout<<a[i]<<" ";
	}
	cout<<endl;
	cout<<"Number of comparison :- "<<comp<<endl;
}